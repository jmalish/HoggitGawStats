# HoggitGawStats

#### Prereqs:
- node
- npm
- run `npm -g i @angular/cli@latest` to install the latest version of angular
- nodemon is not required, but recommended

#### Quick start:
- Create a `secrets.json` file using the format from `sampleSecrets.json`
- run `npm install` to install all node modules

###### Backend / Node server
- Enter server directory
- run `npm i` to install node modules
- run `npx tsc` to build files
- run `node dist/server` to start localhost server (can also use `nodemon` if installed)
  - running `npm start` will build files and start the server using nodemon

###### Front End / Angular
- Enter angular directory
- run `npm i` to install node modules
- run `ng serve`
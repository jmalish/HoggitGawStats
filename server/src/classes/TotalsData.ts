class TotalsData {
    totalPlayers = {};
    weaponsTotals = {};
    killsTotals = {};
    aircraftTotals = {};
    lossesTotals = {};

    constructor(_players, _weapons, _kills, _aircraft, _losses) {
        this.totalPlayers = _players;
        this.weaponsTotals = _weapons;
        this.killsTotals = _kills;
        this.aircraftTotals = _aircraft;
        this.lossesTotals = _losses;
    }
}

module.exports = TotalsData;
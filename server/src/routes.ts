import * as express from 'express';
import * as fs from 'fs';
import * as formidable from 'formidable';
import * as path from 'path';

let MergedPilot = require('./classes/MergedPilot');
let Pilot = require('./classes/Pilot');
let TeamKill = require('./classes/TeamKill');
let TotalsData = require('./classes/TotalsData');

let router = express.Router();

let mergedPilots: MergedPilot[] = [];
let pilots: Pilot[] = [];
let playersData = [];
let teamKills: TeamKill[] = [];
let totalsData = new TotalsData();

// <editor-fold desc='Startup'>
checkForDataDir(_ => {
    let dataDir = 'data/';
    let nameFile = 'name_data.csv';
    let teamKillsFile = 'teamkills.csv';
    let playerDataFile = 'player_data.csv';

    // <editor-fold desc='file checking'>
    // I'm nesting these file checks it'll me to make sure all files are read before continuing
    if (fs.existsSync(dataDir + nameFile)) {                                    // names file
        console.log(nameFile + ' file found, parsing...');
        readNamesFile(_ => {

            if (fs.existsSync(dataDir + playerDataFile)) {                     // player data file
                console.log(playerDataFile + ' file found, parsing...');
                readPlayerDataFile(_ => {

                    mergePilotFiles(_ => {});

                    if (fs.existsSync(dataDir + teamKillsFile)) {              // team kills file
                        console.log(teamKillsFile + ' file found, parsing...');
                        readTeamKillsFile(_ => {
                        });
                    } else {
                        console.log(teamKillsFile + ' file not found!');
                    }
                });
            } else {
                console.log(playerDataFile + ' file not found!');
            }
        });
    } else {
        console.log(nameFile + ' file not found!');
    }
    // </editor-fold desc='file checking'>
});
// </editor-fold desc='Startup'>

// <editor-fold desc='Routes'>
router.get('/api', function (req, res, next) {
    res.send('API is live!');
});

    // <editor-fold desc='upload routes'>
router.get('/api/upload', function (req, res) {
    res.writeHead(200, {'content-type': 'text/html'});
    res.end(
        '<form action="/upload" enctype="multipart/form-data" method="post">'+
        '<input type="file" name="upload" multiple="multiple" accept="text/csv"><br>'+
        '<input type="submit" value="Upload">'+
        '</form>'
    );
});

router.post('/api/upload', function (req, res) {
    let form = new formidable.IncomingForm();
    let recFiles: string[] = [];

    let resString = 'Files received: <br>';

    form.multiples = true;
    form.parse(req);

    form.on('fileBegin', function (name, file) { // called when file is detected
        if (file.name === 'name_data.csv') {
            resString += file.name + '<br>';
            recFiles.push(file.name);

            file.path = path.join(__dirname, '/../data/', file.name);
        } else if (file.name === 'teamkills.csv') {
            resString += file.name + '<br>';
            recFiles.push(file.name);

            file.path = path.join(__dirname, '/../data/', file.name);
        } else if (file.name === 'player_data.csv') {
            resString += file.name + '<br>';
            recFiles.push(file.name);

            file.path = path.join(__dirname, '/../data/', file.name);
        } else {
            console.log(file.name + ' is not a valid file, ignoring.');
        }
    });

    form.on('end', function () { // called when entire request has been received
        recFiles.forEach(fileName => {
            if (fileName === 'name_data.csv') {
                readNamesFile(_ => {});
            } else if (fileName === 'teamkills.csv') {
                readTeamKillsFile(_ => {});
            } else if (fileName === 'player_data.csv') {
                readPlayerDataFile(_ => {})
            }
        });


        res.writeHead(200, {'content-type': 'text/html'});
        res.end(
            '<p>' + resString +  '</p>' +
            '<a href="upload">Return to uploads page</a><br>' +
            '<a href="pilots">Check pilots api endpoint</a>'
        );
    });
});
    // </editor-fold desc='upload routes'>

    // <editor-fold desc='regular api routes'>
router.get('/api/pilots', function (req, res, next) {
    if (req.query['search'] != undefined) {
        searchPilots(req.query['search'], pilots => {
            return res.json(pilots);
        });
    } else if (req.query['ucid'] != undefined) {
        res.json(mergedPilots.filter(pilot => pilot.player['uID'] === req.query['ucid']));
    } else {
        res.json(mergedPilots);
    }
});

router.get('/api/pilot/:ucid', function (req, res, next) {
    let pilotUcid = req.params.ucid;

    let findPilot = mergedPilots.find(function (pilot) {
        return pilot.player['uID'] === pilotUcid;
    });

    res.json(findPilot);
});

router.get('/api/playerData/totals', function (req, res, next) {
    res.json(totalsData);
});

router.get('/api/playerData/:ucid', function (req, res, next) {
    let pilotUcid = req.params.ucid;

    let foundPilot = mergedPilots.find(function (playerData) {
        return playerData.player['uID'] === pilotUcid;
    });

    res.json(foundPilot);
});
    // </editor-fold desc='regular api routes'>
// </editor-fold desc='Routes'>

// <editor-fold desc='Functions'>
    // <editor-fold desc='read files'>
function readNamesFile(callback) {
    let nameFile = 'data/name_data.csv';

    pilots = []; // empty pilots array

    fs.readFile(nameFile, 'utf8', function (err, data) { // read file
        let rows = data.split('\r\n'); // split file into rows

        rows.forEach(row => { // for each row
            let newPilot = new Pilot(); // create new Pilot object
            if (row[0] != undefined) { // make sure we don't grab an empty line
                let columns = row.split(','); // split row into columns

                if (columns[0] === 'uID') return; // skip headers line

                newPilot.name = ''; // set pilot name to a blank string for the sake of the object, set later
                newPilot.ucid = columns[0]; // set pilot ucid
                newPilot.slID = columns[1]; // set pilot slID

                for (let i = columns.length - 1; i > 1; i--) { // for each column
                    if (columns[i] != '0' && columns[i] != '0\r') { // ignore columns that are just a 0
                        if (newPilot.name === '') { // if pilot name hasn't been set
                            newPilot.name = columns[i]; // set it
                        } else {
                            if (columns[i] != newPilot.name) { // make sure this name doesn't match the current name
                                let checkAliases = newPilot.aliases.find(alias => { // make sure this name isn't already in the array
                                    return alias === columns[i];
                                });

                                if (!checkAliases) newPilot.aliases.push(columns[i]); // add it if not
                            }
                        }
                    }
                }
            }

            if (newPilot.ucid != undefined) { // if ucid is undefined, it's probably a blank line, so ignore it
                pilots.push(newPilot); // add new pilot to array
            }
        });
        callback(true);
    });
}

function readTeamKillsFile(callback) {
    let teamKillsFile = 'data/teamkills.csv';

    teamKills = []; // clear teamKills array

    fs.readFile(teamKillsFile, 'utf8', function (err, data) { // read file
        let rows = data.split('\r\n'); // split file into arrays

        rows.forEach(row => { // for each row
            if (row[0] != undefined) { // make sure we don't grab an empty line
                let columns = row.split(','); // split row into columns

                if (columns[0] === 'uID') return; // skip headers line

                teamKills.push(new TeamKill(columns[0], columns[1], columns[2], columns[3], columns[4], columns[5], columns[6], columns[7], columns[8]));
            }
        });
        callback(true);
    });
}

function readPlayerDataFile(callback) {
    let playerDataFile = 'data/player_data.csv';

    playersData = []; // empty array

    fs.readFile(playerDataFile, 'utf8', function (err, data) { // read file
        let rows = data.split('\r\n'); // split file into rows

        let headers = rows[0].split(','); // get header from the first row

        // local arrays for totals
        let weaponsTotals = {};
        let killsTotals = {};
        let aircraftTotals = {};
        let lossesTotals = {};

        rows.forEach(row => { // for each row
            if (row != rows[0]) { // skip header row
                // local arrays
                let player = {};
                let weapons = {};
                let kills = {};
                let aircraft = {};
                let losses = {};

                let columns = row.split(','); // split row into columns

                for (let i = 0; i < columns.length - 1; i++) { // for each column
                    if (columns[i] != '0' && columns[i] != undefined) { // skip the cell if 0 to save space, undefined means a blank line, so ignore those too
                        let header = headers[i];

                        if (header.match(/\bweapon/)) { // isWeapon
                            if (header.indexOf('shot') > -1) {
                                let shotReg = header.match(/weapon(.+)(shot)/i);

                                header = shotReg[1] + ' shot';
                            } else if (header.indexOf('kills') > -1) {
                                let killsReg = header.match(/weapon(.+)(kills)/i);

                                header = killsReg[1] + ' kills';
                            } else if (header.indexOf('numHits') > -1) {
                                let numHitsReg = header.match(/weapon(.+)(numHits)/i);

                                header = numHitsReg[1] + ' number of hits';
                            } else if (header.indexOf('hit') > -1) {
                                let hitReg = header.match(/weapon(.+)(hit)/i);

                                header = hitReg[1] + ' hits';
                            }

                            weapons[header] = columns[i]; // add column to weapons array

                            if (weaponsTotals[header] === undefined) { // if weaponsTotals is empty, we need to tell it it's a number
                                weaponsTotals[header] = 0; // set it to 0
                            }
                            weaponsTotals[header] += parseInt(columns[i]); // add this value to the correlating array location
                        } else if (header.match(/kills/)) { // isKills
                            if (header.indexOf('Ground Unit') > -1) {
                                let grUnReg = header.match(/(kills)(Ground Units)([a-z]+)/i);

                                if (grUnReg[3] === 'total') {
                                    header = 'Ground Units Total';
                                } else
                                {
                                    header = 'Ground Units - ' + grUnReg[3];
                                }
                            } else if (header.indexOf('team') > -1) {
                                header = 'Team Kills';
                            }
                            else if (header.indexOf('total') > -1) {
                                let totalReg = header.match(/kills([a-z]+)total/i);

                                header = totalReg[1] + ' Total';
                            } else {
                                let otherReg = header.match(/kills([A-Z][a-z]+)([A-Z][a-z]+)/);

                                header = otherReg[1] + ' - ' + otherReg[2];
                            }

                            kills[header] = columns[i];

                            if (killsTotals[header] === undefined) {
                                killsTotals[header] = 0;
                            }
                            killsTotals[header] += parseInt(columns[i]);
                        } else if (header.match(/Time/)) { // isAircraft
                            if (header.indexOf('InAir') > -1) {
                                let inAirReg = header.match(/(.+)(Time)(.+)/i);

                                header = inAirReg[1] + ' time in air';

                            } else if (header.indexOf('Total') > -1) {
                                let totalReg = header.match(/(.+)(Time)(.+)/i);

                                header = totalReg[1] + ' time total';
                            }


                            aircraft[header] = columns[i];

                            if (aircraftTotals[header] === undefined) {
                                aircraftTotals[header] = 0.00;
                            }
                            aircraftTotals[header] += parseFloat(columns[i]);
                        } else if (header.match(/loss/)) { // isLoss
                            if (header.indexOf('pilotDeath') > -1) {
                                header = 'Pilot Death';
                            } else if (header.indexOf('crash') > -1) {
                                header = 'Crash';
                            } else {
                                header = 'Eject';
                            }

                            losses[header] = columns[i];

                            if (lossesTotals[header] === undefined) {
                                lossesTotals[header] = 0;
                            }
                            lossesTotals[header] += parseInt(columns[i]);
                        } else { // otherwise it's player data (uID, and slID)
                            player[header] = columns[i];
                        }
                    }
                }

                const newPlayerData = { // add arrays to object
                    player,
                    weapons,
                    kills,
                    aircraft,
                    losses
                };

                playersData.push(newPlayerData); // add this playerData to array
            }
        });


        let totalPlayers = playersData.length;

        totalsData = new TotalsData(
            totalPlayers,
            weaponsTotals,
            killsTotals,
            aircraftTotals,
            lossesTotals);

        callback(true);
    });
}
    // </editor-fold desc='read files'>

function searchPilots(searchTerm: string, callback) {
    searchTerm = searchTerm.toLowerCase();

    let foundPilots: MergedPilot[] = mergedPilots.filter(pilot => {
        if (pilot.player['aliases'] != undefined) {
            if (pilot.player['aliases'].length > 0) { // if pilot has at least one alias
                let aliasTest = pilot.player['aliases'].filter(alias => {
                    return alias.toLowerCase().indexOf(searchTerm) > -1;
                });

                if (aliasTest.length > 0) {
                    return true;
                }
            }
        }

        if (pilot.player['name'] != undefined) {
            return pilot.player['name'].toLowerCase().indexOf(searchTerm) > -1
        } else {
            return false;
        }
    });

    callback(foundPilots);}

function checkForDataDir(callback) {
    if (!fs.existsSync('./data/')) {
        console.log('data directory not found, creating it now');
        fs.mkdirSync('data');
    }

    callback(true);
}

function mergePilotFiles(callback) {
    playersData.forEach(pd => {
        if (pd.player.uID === undefined) return;

        let newMergedPilot = new MergedPilot();

        newMergedPilot = pd;

        let matchingPilot = pilots.find(pilot => pilot.ucid === newMergedPilot.player['uID']);

        if (matchingPilot != undefined) {
            newMergedPilot.player['name'] = matchingPilot.name;
            if (matchingPilot.aliases.length > 0) newMergedPilot.player['aliases'] = matchingPilot.aliases;
        }

        mergedPilots.push(newMergedPilot);
    });

    callback(true);
}
// </editor-fold desc='Functions'>

module.exports = router; // export router
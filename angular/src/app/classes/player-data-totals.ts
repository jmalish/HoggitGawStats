export class PlayerDataTotals {
  totalPlayers: number;
  aircraftTotals: any;
  aircraftTotalsKeys: string[];
  killsTotals: any;
  killsTotalsKeys: string[];
  lossesTotals: any;
  lossesTotalsKeys: string[];
  weaponsTotals: any;
  weaponsTotalsKeys: string[];

  constructor() {
  }
}

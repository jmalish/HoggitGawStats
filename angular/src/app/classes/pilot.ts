import {compareNumbers} from '@angular/compiler-cli/src/diagnostics/typescript_version';

export class Pilot {
  player = {
    name: undefined,
    uID: undefined,
    aliases: []
  };
  aircraft = {
    flightTimeTotal: 0.0
  };
  aircraftKeys = [];
  kills = {
    killsTotal: 0
  };
  killsKeys = [];
  losses = {};
  lossesKeys = [];
  weapons = {};
  weaponsKeys = [];

  constructor() {
  }
}

import { Component, OnInit } from '@angular/core';
import {Observable, of, Subject} from 'rxjs/index';
import {Pilot} from '../classes/pilot';
import {PilotsService} from '../services/pilots.service';
import {debounceTime, distinctUntilChanged, map, switchMap} from 'rxjs/internal/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-pilots',
  templateUrl: './pilots.component.html',
  styleUrls: ['./pilots.component.css']
})
export class PilotsComponent implements OnInit {
  allPilots: Pilot[] = [];
  pilots$: Observable<Pilot[]>;
  private searchTerms = new Subject<string>();
  isSearching = false;
  initialSearched = false;

  constructor(
    private pilotService: PilotsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getPilots();

    this.pilots$ = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) => this.pilotService.searchPilots(term)),
      map(pilots => this.sortPilots(pilots))
    );
  }

  search(term: string): void {
    this.searchTerms.next(term);

    this.initialSearched = true;
    this.isSearching = term !== '';
  }

  goToPilot(ucid: string): void {
    this.router.navigate(['/pilot', ucid]).then(_ => {});
  }

  getPilots(): void {
    this.pilotService.getAllPilots().subscribe(pilots => {
      this.allPilots = this.sortPilots(pilots);
    });
  }

  sortPilots(pilots: Pilot[]): any {
    return pilots.sort((a, b) => {
      let aName = a.player.name;
      let bName = b.player.name;

      if (aName === undefined || bName === undefined) { return 0; }

      aName = aName.toLowerCase().match(/([a-z0-9]+)/i);
      bName = bName.toLowerCase().match(/([a-z0-9]+)/i);

      if (aName > bName) {
        return 1;
      } else {
        return -1;
      }
    });
  }
}

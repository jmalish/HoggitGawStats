import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
// import {DashboardComponent} from './dashboard/dashboard.component';
import {PilotDetailComponent} from './pilot-detail/pilot-detail.component';
import {PilotsComponent} from './pilots/pilots.component';
import {PlayerDataTotalsComponent} from './player-data-totals/player-data-totals.component';
import {LeaderBoardsComponent} from './leader-boards/leader-boards.component';

const routes: Routes = [
  {path: '', redirectTo: '/pilots', pathMatch: 'full'},
  {path: 'pilots', component: PilotsComponent},
  {path: 'pilot/:ucid', component: PilotDetailComponent},
  {path: 'totals', component: PlayerDataTotalsComponent},
  {path: 'leaderboards', component: LeaderBoardsComponent}
];

@NgModule({
  exports: [RouterModule],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }

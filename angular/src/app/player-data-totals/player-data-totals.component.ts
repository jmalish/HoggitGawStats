import { Component, OnInit } from '@angular/core';
import {PlayerDataTotals} from '../classes/player-data-totals';
import {PilotsService} from '../services/pilots.service';

@Component({
  selector: 'app-player-data-totals',
  templateUrl: './player-data-totals.component.html',
  styleUrls: ['./player-data-totals.component.css']
})
export class PlayerDataTotalsComponent implements OnInit {
  playerDataTotals = new PlayerDataTotals();
  showLosses = true;
  lossesButton = 'Hide';
  showAircraft = true;
  aircraftButton = 'Hide';
  showKills = true;
  killsButton = 'Hide';
  showWeapons = true;
  weaponsButton = 'Hide';

  constructor(
    private pilotsService: PilotsService
  ) { }

  ngOnInit() {
    this.playerDataTotals.aircraftTotalsKeys = [];

    this.getTotals();
  }

  getTotals(): void {
    this.pilotsService.getTotals().subscribe(pdt => {
      this.playerDataTotals = pdt;
      this.playerDataTotals.aircraftTotalsKeys = Object.keys(pdt[`aircraftTotals`]);
      this.playerDataTotals.lossesTotalsKeys = Object.keys(pdt[`lossesTotals`]);
      this.playerDataTotals.killsTotalsKeys = Object.keys(pdt[`killsTotals`]);
      this.playerDataTotals.weaponsTotalsKeys = Object.keys(pdt[`weaponsTotals`]);
    });
  }

  toggleLosses() {
    this.showLosses ? this.showLosses = false : this.showLosses = true;
    this.showLosses ? this.lossesButton = 'Hide' : this.lossesButton = 'Show';
  }

  toggleAircraft() {
    this.showAircraft ? this.showAircraft = false : this.showAircraft = true;
    this.showAircraft ? this.aircraftButton = 'Hide' : this.aircraftButton = 'Show';
  }

  toggleKills() {
    this.showKills ? this.showKills = false : this.showKills = true;
    this.showKills ? this.killsButton = 'Hide' : this.killsButton = 'Show';
  }

  toggleWeapons() {
    this.showWeapons ? this.showWeapons = false : this.showWeapons = true;
    this.showWeapons ? this.weaponsButton = 'Hide' : this.weaponsButton = 'Show';
  }
}

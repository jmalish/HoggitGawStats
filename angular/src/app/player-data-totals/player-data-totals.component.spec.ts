import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerDataTotalsComponent } from './player-data-totals.component';

describe('PlayerDataTotalsComponent', () => {
  let component: PlayerDataTotalsComponent;
  let fixture: ComponentFixture<PlayerDataTotalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerDataTotalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerDataTotalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

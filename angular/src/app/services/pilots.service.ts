import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Pilot} from '../classes/pilot';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import * as secrets from '../../../../secrets.json';

@Injectable({
  providedIn: 'root'
})
export class PilotsService {
  private apiUrl = 'http://' + secrets.server.url + ':' + secrets.server.port + '/api';

  constructor(
    private http: HttpClient
  ) { }

  getPilotByUcid(ucid: string): Observable<Pilot> {
    return this.http.get<any>(this.apiUrl + '/pilot/' + ucid).pipe(
      map(res => {
        return res;
      })
    );
  }

  searchPilots(name: string): Observable<Pilot[]> {
    console.log(`${this.apiUrl}/pilots?search=${name}`);
    if (!name.trim()) {
      return this.getAllPilots();
    }
    return this.http.get<Pilot[]>(`${this.apiUrl}/pilots?search=${name}`);
  }

  getAllPilots(): Observable<Pilot[]> {
    return this.http.get<Pilot[]>(`${this.apiUrl}/pilots`);
  }

  getTotals(): Observable<any> {
    return this.http.get<any>(this.apiUrl + '/playerData/totals').pipe(
      map(res => {
        return res;
      })
    );
  }
} // end of PilotService class

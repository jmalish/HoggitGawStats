import { Component, OnInit, OnDestroy } from '@angular/core';
import {Pilot} from '../classes/pilot';
import {ActivatedRoute} from '@angular/router';
import {PilotsService} from '../services/pilots.service';

@Component({
  selector: 'app-pilot-detail',
  templateUrl: './pilot-detail.component.html',
  styleUrls: ['./pilot-detail.component.css']
})
export class PilotDetailComponent implements OnInit, OnDestroy {
  ucid: string;
  private sub: any;
  pilot: Pilot = new Pilot();
  aliasCount = 0;

  constructor(
    private route: ActivatedRoute,
    private pilotService: PilotsService
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const key = 'ucid';
      this.ucid = params[key];
      this.getPilotDetails();
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getPilotDetails() {
    this.pilotService.getPilotByUcid(this.ucid).subscribe(pilot => {
      this.pilot = pilot;

      if (this.pilot.player.aliases !== undefined) { this.aliasCount = this.pilot.player.aliases.length; }

      this.pilot.aircraft = pilot.aircraft;
      this.pilot.aircraftKeys = Object.keys(pilot.aircraft);
      this.pilot.kills = pilot.kills;
      this.pilot.killsKeys = Object.keys(pilot.kills);
      this.pilot.losses = pilot.losses;
      this.pilot.lossesKeys = Object.keys(pilot.losses);
      this.pilot.weapons = pilot.weapons;
      this.pilot.weaponsKeys = Object.keys(pilot.weapons);
    });
  }
}

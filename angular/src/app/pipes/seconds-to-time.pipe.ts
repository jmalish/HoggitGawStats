import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'secondsToTime'
})
export class SecondsToTimePipe implements PipeTransform {
  transform(_seconds: number): string {
    let finalStr = '';
    _seconds = Math.round(_seconds);

    if (_seconds < 60) { // less a minute
      finalStr = _seconds + ' seconds';

    } else if (_seconds >= 60 && _seconds < 3600) { // less than an hour
      let seconds = _seconds % 60;
      let minutes = (_seconds - seconds)/60;

      finalStr = minutes + ' minute';
      if (minutes > 1) finalStr += 's';
      if (seconds > 0) finalStr += ', ' + seconds + ' second';
      if (seconds > 1) finalStr += 's';
    } else if (_seconds >= 3600) { // more than one hour
      let seconds = _seconds % 60;
      let minutes = ((_seconds - seconds) / 60) % 60;
      let hours = (_seconds - seconds - (minutes * 60)) / 60 / 60;

      finalStr = hours + ' hour';
      if (hours > 1) finalStr += 's';
      if (minutes > 0) finalStr += ', ' + minutes + ' minute';
      if (minutes > 1) finalStr += 's';
      if (seconds > 0) finalStr += ', ' + seconds + ' second';
      if (seconds > 1) finalStr += 's';
    }

    return finalStr;
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import { PilotDetailComponent } from './pilot-detail/pilot-detail.component';
import {FormsModule} from '@angular/forms';
import { PilotsComponent } from './pilots/pilots.component';
import { PlayerDataTotalsComponent } from './player-data-totals/player-data-totals.component';
import { SecondsToTimePipe } from './pipes/seconds-to-time.pipe';
import { LeaderBoardsComponent } from './leader-boards/leader-boards.component';

@NgModule({
  declarations: [
    AppComponent,
    PilotDetailComponent,
    PilotsComponent,
    PlayerDataTotalsComponent,
    SecondsToTimePipe,
    LeaderBoardsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

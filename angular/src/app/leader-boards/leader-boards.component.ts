import { Component, OnInit } from '@angular/core';
import {PilotsService} from '../services/pilots.service';
import {Pilot} from '../classes/pilot';

@Component({
  selector: 'app-leader-boards',
  templateUrl: './leader-boards.component.html',
  styleUrls: ['./leader-boards.component.css']
})
export class LeaderBoardsComponent implements OnInit {
  allPilots: Pilot[] = [];
  killsLeaders: Pilot[] = [];
  flightTimeLeaders: Pilot[] = [];
  killsLeadersToShow = 10;
  flightLeadersToShow = 10;
  aircraftShowMoreBtnTxt = 'more';
  killShowMoreBtnTxt = 'more';

  constructor(
    private pilotService: PilotsService
  ) { }

  ngOnInit() {
    this.getPilots();
  }

  getPilots(): void {
    this.pilotService.getAllPilots().subscribe(pilots => {
      this.allPilots = pilots;
      this.killsLeaders = this.sortPilotsByKills(this.allPilots);
      this.flightTimeLeaders = this.sortPilotsByFlightTime(this.allPilots);
    });
  }

  sortPilotsByKills(pilots: Pilot[]): any {
    const cloned = pilots.map(x => Object.assign({}, x));

    return cloned.sort((a, b) => {
      let aKillsTotal = 0;
      let bKillsTotal = 0;

      a.killsKeys = Object.keys(a.kills);
      b.killsKeys = Object.keys(b.kills);

      a.killsKeys.forEach(key => {
        if (key.toLowerCase().indexOf('total') > -1) {
          if (key !== 'killsTotal') {
            aKillsTotal += parseInt(a.kills[key], 10);
          }
        }
      });

      b.killsKeys.forEach(key => {
        if (key.toLowerCase().indexOf('total') > -1) {
          if (key !== 'killsTotal') {
            bKillsTotal += parseInt(b.kills[key], 10);
          }
        }
      });

      a.kills.killsTotal = aKillsTotal;
      b.kills.killsTotal = bKillsTotal;

      if (aKillsTotal < bKillsTotal) {
        return 1;
      } else {
        return -1;
      }
    });
  }

  sortPilotsByFlightTime(pilots: Pilot[]): any {
    const cloned = pilots.map(x => Object.assign({}, x));

    return cloned.sort((a, b) => {
      let aFlightTime = 0;
      let bFlightTime = 0;

      a.aircraftKeys = Object.keys(a.aircraft);
      b.aircraftKeys = Object.keys(b.aircraft);

      a.aircraftKeys.forEach(key => {
        if (key.toLowerCase().indexOf('time in air') > -1) {
          aFlightTime += parseFloat(a.aircraft[key]);
        }
      });

      b.aircraftKeys.forEach(key => {
        if (key.toLowerCase().indexOf('time in air') > -1) {
          bFlightTime += parseFloat(b.aircraft[key]);
        }
      });

      a.aircraft.flightTimeTotal = aFlightTime;
      b.aircraft.flightTimeTotal = bFlightTime;

      if (aFlightTime < bFlightTime) {
        return 1;
      } else {
        return -1;
      }
    });
  }

  toggleMoreKills() {
    if (this.killsLeadersToShow === 10) {
      this.killsLeadersToShow = 50;
      this.killShowMoreBtnTxt = 'less';
    } else {
      this.killsLeadersToShow = 10;
      this.killShowMoreBtnTxt = 'more';
    }
  }

  toggleMoreAircraftLeaders() {
    if (this.flightLeadersToShow === 10) {
      this.flightLeadersToShow = 50;
      this.aircraftShowMoreBtnTxt = 'less';
    } else {
      this.flightLeadersToShow = 10;
      this.aircraftShowMoreBtnTxt = 'more';
    }
  }
}
